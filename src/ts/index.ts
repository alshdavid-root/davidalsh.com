import { throttle } from 'lodash'
import { setNavFromScroll, isMobileResolution } from './platform';

void async function main() {
    if (!isMobileResolution()) {
        setNavFromScroll()
        window.addEventListener('scroll', throttle(setNavFromScroll, 100));
    }
}()