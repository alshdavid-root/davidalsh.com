export const getPosition = (element: any) => {
    var yPosition = 0;
    let hasParent = true

    while(hasParent) {
        yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
        if (element.offsetParent === null) {
            hasParent = false
            continue
        };
        element = element.offsetParent 
    }

    return yPosition;
}