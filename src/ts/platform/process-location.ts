import { getPosition } from './get-position'

interface ILocation {
    key: string 
    pos: number
}

const setNavItem = (item: string) => {
    document.querySelectorAll('nav ul:first-child a')
        .forEach(element => {
            element.classList.remove('active')
            if (element.getAttribute('href') === '#' + item) {
                element.classList.add('active')
            }
        })
}

const findLocationOfIDs = (): ILocation[] => {
    return Array.from(document.querySelectorAll('[id]')).map((e) => {
        return { 
            key: e.getAttribute('id') || '', 
            pos: getPosition(e)
        }
    })
}

const whichSection = (locations: ILocation[], focus: number) => {
    for (let i = 0; i < locations.length; i++) {
        const start = locations[i].pos
        const end = (i != locations.length - 1) ? locations[i+1].pos : document.body.scrollHeight

        if (focus >= start && focus <= end) {
            return locations[i].key
        }
    }
}

export const setNavFromScroll = () => {
    const topOfScroll = window.pageYOffset
    const focus = (topOfScroll + window.innerHeight) - (window.innerHeight * 0.5)
 
    const locations = findLocationOfIDs()
    const chosen = whichSection(locations, focus)
    if (chosen === undefined) {
        return
    }
    setNavItem(chosen)
}