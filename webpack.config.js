const path = require('path');

const postCSSOptions = {
    plugins: [
        require('autoprefixer')
    ]
}

const mode = process.argv.includes('--prod') ? 'production' : 'development'

module.exports = {
    entry: [
        path.join(__dirname, '/src/ts/index.ts'),
        path.join(__dirname, '/src/scss/styles.scss')
    ],
    mode: mode,
    watchOptions: {
        aggregateTimeout: 300,
        poll: 1000
    },
    output: {
        filename: 'index.js',
        path: path.join(__dirname, 'public')
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.scss$/,
                use: [
                    { loader: "style-loader" },
                    { loader: "css-loader" },
                    { loader: "postcss-loader", options: postCSSOptions },
                    { loader: "sass-loader" },
                ]
            }
        ]
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js"]
    },
};